function(collection, query) {
    return db[collection].find(query).map((doc) => {

        var totalRecurseSize = 5;

        function getList(_id) {
            return db.lists.findOne({_id: _id}).LIST;
        }

        function getBody(_id) {
            return db.body.findOne({_id: _id});
        }

        function getValue(val, index) {
            if(index === 0) return null;
            if(typeof val === 'object') {
                if(val.$ref && val.$ref === 'lists') {
                    return getList(val.$id).map(listItem => getValue(listItem, index ? index-1 : totalRecurseSize));
                } else if(val.$ref) {
                    return getDoc(getBody(val.$id), index ? index-1 : totalRecurseSize);
                } else if(val.map) {
                    return val.map(listItem => {
                        return getValue(listItem, index ? index-1 : totalRecurseSize)
                    })
                }
            } else {
                return val;
            }
        }

        function getDoc(item, index){
            var x = {};
            if(index === 0) return null;
            for(var some in item) {
                x[some === '_id' ? 'id' : some] = getValue(item[some]);
            }
            return x
        }

        return getDoc(doc);
    })
}