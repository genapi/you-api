package com.genapi.youapi.changelogs.mongo;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.DBRef;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.io.IOUtils;
import org.bson.BsonDocument;
import org.bson.BsonJavaScript;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@ChangeLog
public class InitMongoChangelog {

    @ChangeSet(order = "001", id = "createBody", author = "BurlakovEM")
    public void createBody(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("body");
        Document doc1 = new Document()
                .append("_id", 1)
                .append("name", "piter")
                .append("fieldX", "testValue");
        Document doc2 = new Document()
                .append("_id", 2)
                .append("name", "viktor")
                .append("modelField", new DBRef("body", doc1.get("_id")));
        collection.insertOne(doc1);
        collection.insertOne(doc2);
    }

    @ChangeSet(order = "002", id = "createLits", author = "BurlakovEM")
    public void createArrays(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("lists");
    }

    @ChangeSet(order = "003", id = "createMapFunction", author = "BurlakovEM")
    public void creteMapFunction(MongoDatabase db) throws IOException {
        MongoCollection<Document> collection = db.getCollection("system.js");
        BasicDBObject query = new BasicDBObject();
        query.put("_id", "mapApiDocs");

        if(collection.find(query).first() != null) return;

        Resource resource = new ClassPathResource("mongo/functions/map-api-docs.js");
        InputStream input = resource.getInputStream();

        Document mapApiDocsFunction = new Document();
        mapApiDocsFunction.put("_id", "mapApiDocs");
        mapApiDocsFunction.put("value",
                new BsonJavaScript(IOUtils.toString(input, StandardCharsets.UTF_8.toString())));

        db.getCollection("system.js").insertOne(mapApiDocsFunction);
        db.runCommand(new Document("$eval", "db.loadServerScripts()"));
    }

}
