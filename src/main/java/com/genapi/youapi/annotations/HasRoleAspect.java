package com.genapi.youapi.annotations;

import com.genapi.youapi.enums.RolesEnum;
import com.genapi.youapi.utils.SecurityUtility;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;

@Aspect
@Component
public class HasRoleAspect {

    @Autowired
    SecurityUtility securityUtility;

    @Pointcut("@annotation(com.genapi.youapi.annotations.HasRole)")
    private void hasRoleAnnotation() {
    }

    @Around("@annotation(hr)")
    public Object doSomething(ProceedingJoinPoint pjp, HasRole hr) throws Throwable {

        RolesEnum[] roles = hr.value();
        if (roles == null || roles.length == 0) {
            return null;
        }

        boolean userHasRole = securityUtility.containsRole(roles);
        if (!userHasRole) {
            return ResponseEntity.status(HttpServletResponse.SC_FORBIDDEN).build();
        }

        return pjp.proceed();
    }

}
