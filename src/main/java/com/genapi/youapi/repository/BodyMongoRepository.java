package com.genapi.youapi.repository;

import com.genapi.youapi.client.MongoDBClient;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BodyMongoRepository extends MongoRepository {

    @Autowired
    public BodyMongoRepository(MongoDBClient mongoDBClient) {
        this.mongoDBClient = mongoDBClient;
        this.collectionName = "body";
    }

}
