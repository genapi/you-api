package com.genapi.youapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@EnableConfigurationProperties(MongoProperties.class)
@SpringBootApplication
public class YouApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(YouApiApplication.class, args);
    }

}
