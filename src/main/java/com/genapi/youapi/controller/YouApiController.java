package com.genapi.youapi.controller;

import com.genapi.youapi.client.feign.BackRoutesClient;
import com.genapi.youapi.enums.RolesEnum;
import com.genapi.youapi.service.BodyPreviewService;
import com.genapi.youapi.dto.RouteDTO;
import com.genapi.youapi.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@RequestMapping("/api")
@RestController
public class YouApiController {

    private final BackRoutesClient backRoutesClient;
    private final BodyPreviewService bodyPreviewService;
    private final Servlet servlet;
    private final SecurityUtility securityUtility;

    @GetMapping("/{projectId}/**")
    public ResponseEntity<Object> getDocument(HttpServletRequest req, @PathVariable Long projectId) {
        String query = req.getRequestURI();
        String contextPath = servlet.getServletConfig().getServletContext().getContextPath();
        query = query.replaceFirst (contextPath + "/api/" + projectId, "");
        RouteDTO routeDTO = backRoutesClient.getViewByRouteAndProjectId(query, projectId);

        if(routeDTO == null || !routeDTO.isActive()) {
            return ResponseEntity.notFound().build();
        }

        if(routeDTO.isAuth()) {

            if(!securityUtility.isAuthenticated()) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            if(securityUtility.containsRole(RolesEnum.PROJECT_USER) && (securityUtility.getUserProjectId() == null || !routeDTO.getProjectId().equals(securityUtility.getUserProjectId()))) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            if(securityUtility.containsRole(RolesEnum.USER) && (securityUtility.getUserId() == null || !routeDTO.getProjectOwner().equals(securityUtility.getUserId()))) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

        }

        if(routeDTO.getBodyId() != null) {
            return ResponseEntity.ok(bodyPreviewService.getPreviewById((int) (long) routeDTO.getBodyId()));
        } else if(routeDTO.getListId() != null) {
            return ResponseEntity.ok(bodyPreviewService.getPreviewListById((int) (long) routeDTO.getListId()));
        }

        return ResponseEntity.notFound().build();
    }

}
